import cv2
import numpy as np
import time

images = []
cap = cv2.VideoCapture(0)

while True:
    ret, img = cap.read()
    if not ret:
        continue

    keyIpt = cv2.waitKey(1)

    if(keyIpt == ord('s')):
        images.append(img) #saves image
        time.sleep(0.2)
    
    img = cv2.resize(img, (1280, 720))
    img = cv2.flip(img, 1)
    cv2.imshow("image", img)

    if(keyIpt == ord('q')): break #quit

images = np.array(images)
np.save("images2.npy", images)